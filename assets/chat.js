var socket = io()

function submitfunction() {
  let from = $('#user').val()
  let message = $('#m').val()
  if (message != '') {
    socket.emit('chatMessage', from, message)
  }
   $('#m').val('').focus()
  return false
}
function notifyTyping() {
    var user = $('#user').val()
    socket.emit('notifyUserTyping', user)
}
   
socket.on('chatMessage', function (from, msg) {
    var me = $('#user').val()
    var color = (from === me) ? 'green' : '#009afd'
    var from = (from === me) ? 'Me' : from
    $('#messages').append('<li><b style="color:' + color + '">' + from + '</b>: ' + msg + '</li>')
})
   
socket.on('notifyUserTyping', function (user) {
    var me = $('#user').val()
    if (user !== me) {
      $('#notifyUserTyping').text(user + ' is typing ...')
    }
    setTimeout(function () { $('#notifyUserTyping').text('') }, 10000)
})
   
$(function () {
    var name = makeid()
    $('#user').val(name)
    socket.emit('chatMessage', 'System', '<b>' + name + '</b> has joined the discussion')
})
   
function makeid() {
    var text = ''
    var possible = 'abcdeghijklmnoprstuwxy'
    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return text
   }
   